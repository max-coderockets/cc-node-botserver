// Dependencies
var apiai = require('apiai');
var express = require('express');
var uuidV1 = require('uuid/v1');
var request = require('request');
var cheerio = require('cheerio');

//Server Code
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var sId;

app.use(express.static(__dirname + '/public'));

/*app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});*/

app.get('/scrap', function(req, res) {
    var options = {
	  url: 'http://www.my-stuwe.de/mensa/mensa-reutlingen/?woche=02',
	  headers: {
	    'User-Agent': 'Mozilla/5.0'
	  }
	};

    request(options, function(error, response, html) {
        if(!error){
            var $ = cheerio.load(html);
            var dayHelper = [[], [0, 1, 4], [5, 6, 9], [10, 11, 14], [15, 16, 19], [20, 21, 24]];
            var dishOfTheDay, alternativeDish, dessert;

            dishOfTheDay = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[5][0]).text();
            alternativeDish = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[5][1]).text();
            dessert = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[5][2]).text();
        }
    });
});

server.listen(process.env.PORT || 8080);

// Bot Code
var bot = apiai("1bb4249057584ad09d9e42ac8c991516");

io.on('connection', function(client) {

	sId = uuidV1();

  	client.on('send message', function(msg) {
		io.emit('new message', {msg: msg, sender: 'user'});

  		botEval(msg).then(function(res) {
  			io.emit('new message', res);
  		});
   	});

});

function botEval(msg) {
	return new Promise(function(resolve, reject) {
		var request = bot.textRequest(msg, {
		    sessionId: sId
		});
		 
		request.on('response', function(response) {
		    console.log(response);

		    switch(response.result.action) {
		    	case 'mensa.fetch.menu':
		    		botFetchMenu(response.result.parameters).then(function(res) {
		    			resolve({msg: res, botResponse: JSON.stringify(response, null, 2), sender: 'bot'});
		    		});
		    		break;

		    	default:
		    		resolve({msg: response.result.fulfillment.speech, botResponse: JSON.stringify(response, null, 2), sender: 'bot'});
		    }
		});
		 
		request.on('error', function(error) {
		    reject(error);
		});
		 
		request.end();	
	});
}

function botFetchMenu(params) {
	return new Promise(function(resolve, reject) {
		var date;

		if (params.date) {
			date = params.date;
		} else {
			var todayIso = new Date().toISOString();
			date = todayIso.slice(0, -14);
			console.log(date);
		}
		
		scrapMenu(new Date(date), params.menu_type).then(function(res) {
			resolve(res);
		}, function(err) {
			resolve(err);
		});
	});
}

// scrapMenu(new Date('2017-01-11'))
function scrapMenu(d, menuType) {
	return new Promise(function(resolve, reject) {
		// Fetch week and day...
		dn = new Date(+d);
	    dn.setHours(0,0,0,0);
	    dn.setDate(dn.getDate() + 4 - (dn.getDay()||7));
	    var yearStart = new Date(dn.getFullYear(),0,1);
	    var weekNo = Math.ceil(( ( (dn - yearStart) / 86400000) + 1)/7);
	    var dayNo = d.getDay();

	    if (dayNo > 5 || dayNo === 0) {
	    	reject('Die Mensa hat am Wochenende ('+formatDate(d)+') geschlossen. Deshalb kann ich für diesen Zeitraum leider keinen Speiseplan abrufen.');
	    	return;
	    }

		if (weekNo < 10) {
			weekNo = '0'+weekNo;
		}

		var options = {
		  url: 'http://www.my-stuwe.de/mensa/mensa-reutlingen/?woche='+weekNo,
		  headers: {
		    'User-Agent': 'Mozilla/5.0'
		  }
		};

	    request(options, function(error, response, html) {
	        if(!error){
	            var $ = cheerio.load(html);
	            var dayHelper;
	            if (weekNo === '02') {
					dayHelper = [[0, 0, 0], [0, 0, 0], [0, 1, 5], [6, 7, 10], [11, 12, 15], [16, 17, 20], [21, 22, 25]];
	            } else {
	            	dayHelper = [[0, 0, 0], [0, 1, 4], [5, 6, 9], [10, 11, 14], [15, 16, 19], [20, 21, 24]];

	            } 
	            var dishOfTheDay, alternativeDish, dessert;
	            var formatedDate = formatDate(d);

	            dishOfTheDay = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[dayNo][0]).text();
	            alternativeDish = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[dayNo][1]).text();
	            dessert = $('.avia-table.speiseplan2 td:nth-child(2)').eq(dayHelper[dayNo][2]).text();

	            if (!menuType) {
	            	if (!dishOfTheDay || !alternativeDish || !dessert) {
	            		reject('Ich konnte den Speiseplan für '+formatedDate+' leider nicht abrufen. Melde dich doch später nochmal bei mir dann versuche ich es erneut.');
	            	} else {
	            		resolve('Am '+formatedDate+' gibt es in der Mensa als Tagesgericht: "'+
							dishOfTheDay+'", als Alternativgericht: "'+
							alternativeDish+'" und zum Dessert: "'+dessert+'"');
	            	}
	            } else {
	            	if (menuType === 'tagesmenü') {
	            		resolve('Am '+formatedDate+' gibt es in der Mensa als Tagesgericht: "'+
							dishOfTheDay+'"');
	            	} else if (menuType === 'alternativmenü') {
	            		resolve('Am '+formatedDate+' gibt es in der Mensa als Alternativgericht: "'+
							alternativeDish+'"');
	            	} else if (menuType === 'dessert') {
	            		resolve('Am '+formatedDate+' gibt es in der Mensa zum Dessert: "'+dessert+'"');
	            	}
	            }  
	        }
	    });
	});
}

function formatDate(date) {
	var weekdays = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
	var months = ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sept', 'Okt', 'Nov', 'Dez'];
	return weekdays[date.getDay()]+' den '+date.getDate()+'. '+months[date.getMonth()]+' '+date.getFullYear();
}

